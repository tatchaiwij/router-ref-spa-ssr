import "./Navbar.css"

import { Link } from 'react-router-dom';

function Navbar () {
    
    return (
        <div className="navbar">
            <Link to='/home'>Home</Link>
            <Link to='/link'>Link</Link>
        </div>
    )
}

export default Navbar;