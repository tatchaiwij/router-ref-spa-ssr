import './App.css';
import Home from "./components/Home";
import Link from "./components/Link";
import Navbar from './components/Navbar';

import { BrowserRouter, Route, Routes, Navigate } from 'react-router-dom';
import { useState } from "react";

function App() {
  return (
    <div>
     <BrowserRouter>
        <Navbar />
        <Routes>
          <Route path="/" element={<Navigate replace to="/home" />} />
          <Route path='/home' index element={<Home />} />
          <Route path='/link' element={<Link />} />
        </Routes>
     </BrowserRouter>
   </div>
  );
}

export default App;
