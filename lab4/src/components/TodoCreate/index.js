function ToDoCreate(props) {

    const handleSubmit = (e) => {
        e.preventDefault();
        props.setItem([...props.items, e.target[0].value]);
    };

    return (
        <div>
            <form onSubmit={handleSubmit}>
                <label>
                    Input:
                    <input type="text" name="item" />
                </label>
                <input type="submit" value="Submit" />
            </form>
        </div>
    )
}

export default ToDoCreate;
