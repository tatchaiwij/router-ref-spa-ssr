import { useNavigate } from "react-router-dom";
import "./Navbar.css"

function Navbar () {
    
    const navigate = useNavigate()
    
    return (
        <div className="navbar">
            <div className="nav" onClick={() => navigate('/home')}>Home</div>
            <div className="nav" onClick={() => navigate('/todo_create')}>ToDo_Create</div>
            <div className="nav" onClick={() => navigate('/todo_lists')}>ToDo_Lists</div>
        </div>
    )
}

export default Navbar;