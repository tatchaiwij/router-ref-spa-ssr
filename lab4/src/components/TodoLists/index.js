function ToDoList (props) {
    return (
        <div>
            {
                props.items.map((item) =>
                    <p key={Math.random()}>{item}</p>
                )
            }
        </div>
    )
}

export default ToDoList;
