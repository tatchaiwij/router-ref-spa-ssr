import './App.css';
import Home from "./components/Home";
import TodoCreate from "./components/TodoCreate";
import TodoLists from "./components/TodoLists";
import Navbar from './components/Navbar';

import { BrowserRouter, Route, Routes, Navigate } from 'react-router-dom';
import { useState } from "react";

function App() {
  const [items, setItem] = useState([])
  return (
    <div>
     <BrowserRouter>
        <Navbar />
        <Routes>
          <Route path="/" element={<Navigate replace to="/home" />} />
          <Route path='/home' index element={<Home />} />
          <Route path='/todo_create' element={<TodoCreate items={items} setItem={setItem} />} />
          <Route path='/todo_lists' element={<TodoLists items={items}/>} />
        </Routes>
     </BrowserRouter>
   </div>
  );
}

export default App;
